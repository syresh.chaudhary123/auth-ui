import './App.css';
import Signup from './components/SignUp/Signup.jsx';
import Login from './components/Login/Login.jsx';
import { useState } from 'react';

function App() {

  const [action, setAction] = useState("Sign Up");
  return (
    <div>
      <div className='action'>
          <button className={action==="Login"?"submit gray":"submit"} onClick={()=>setAction("Sign Up")}>Sign Up</button>
          <button className={action==="Sign Up"?"submit gray":"submit"} onClick={()=>setAction("Login")}>Login</button>
      </div>
     
     
      {
        action==="Login"? <Login/>:<Signup/>
      }
    </div>
  );
}

export default App;
