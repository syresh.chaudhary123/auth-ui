import React, { useState, useEffect } from 'react';
import email_icon from '../Assets/email.png';
import password_icon from '../Assets/password.png';
import axios from 'axios';

const Login = () => {

    const [usersList, setUsersList] = useState([]);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loggedInUser, setLoggedInUser] = useState([])
    

    const handelForm = async(event) => {
      event.preventDefault();

      const user= usersList.filter((user) =>(user.email===email && user.password===password))
      console.log(user)
      if(user.length===0){
        alert("Username or password is incorrect")
      } 
      setLoggedInUser(user)
    }

    const getuserList = async() => {
      const response = await axios.get("http://localhost:3000/users")

      setUsersList(response.data)
    }
const handelLogout =() =>{
  setLoggedInUser([])
}

  useEffect(
    () => {
      getuserList()
    }, []
  )
  return (

    <div className='container'>
      {
        loggedInUser && loggedInUser.length > 0 ? (
          <div>
            <h1>Hello,i am {loggedInUser[0].name}</h1>
            <h2>my email is {loggedInUser[0].email}</h2>
              <div className='action'>
                <button className='my-button' type='submit' onClick={handelLogout}> Logout </button>
              </div>
          </div>
        ) : (
          <form onSubmit={(event) => handelForm(event)}>
          <div className="inputs">
           
            <div className="input">
                <img src={email_icon} alt="" />
                <input type="email" placeholder='Email Id' name='email' onChange={(event) =>setEmail(event.target.value)}/>
            </div>
        
            <div className="input">
                <img src={password_icon} alt="" />
                <input type="password" placeholder='Password' name='password' onChange={(event) =>setPassword(event.target.value)}/>
            </div>
            
          </div>     
          <div className='action'>
            <button className='my-button' type='submit'> Login </button>
          </div>
          

             
        </form> 
        )
      }
        
        
    </div>
     
  );
};
export default Login;