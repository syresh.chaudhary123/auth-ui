import React, { useState } from 'react';
import "./Signup.css";
import user_icon from '../Assets/person.png';
import email_icon from '../Assets/email.png';
import password_icon from '../Assets/password.png';
import axios from 'axios';

const Signup = () => {

  
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");


    const handelForm = async(event) => {
      event.preventDefault();
      console.log(username, email, password)
      if (username && email && password) {
        await axios.post("http://localhost:3000/users",{email:email, name:username, password:password})
        .then((response) =>{
            console.log(response)
            alert("User successfully submitted")
            setUsername("");
            setEmail("");
            setPassword("");
        }).catch(
          (error) =>{
            console.log(error)
          }
        )
      } else {
        alert("Please enter all values")
      }
     
    }

  return (
    <div className='container'>
        
        <div className="submit-container">
              
            </div> 

            
        <form onSubmit={(event) => handelForm(event)}>
          <div className="inputs">
             <div className="input">
                <img src={user_icon} alt="" />
                <input type="text" placeholder='Name' name='name' value={username} onChange={(event) =>setUsername(event.target.value)}/>
            </div>
           
            <div className="input">
                <img src={email_icon} alt="" />
                <input type="email" placeholder='Email Id' name='email' value={email} onChange={(event) =>setEmail(event.target.value)}/>
            </div>
        
            <div className="input">
                <img src={password_icon} alt="" />
                <input type="password" placeholder='Password' name='password' value={password} onChange={(event) =>setPassword(event.target.value)}/>
            </div>
            
          </div>     
          <div className='action'>
            <button className="my-button" type='submit'> Submit </button>
          </div>
          
             
        </form> 
    </div>

  );
};
export default Signup;